package com.epam.passive.model.plane;
import com.epam.passive.model.transport.Transport;

/**
 * Abstract Plane class is the child class of abstract Transport class
 */
public abstract class Plane extends Transport {
    protected int fuselage;
    protected int range;
    protected int payload;

    /**
     * Function to get plane's payLoad.
     * @return
     */
    public int getPayload() { return payload; }

    /**
     * Function to set plane's payLoad.
     * @param payload is the value of payLoad to be set.
     */
    public void setPayload(int payload) {
        this.payload = payload;
    }

    /**
     * Function to get plane's range.
     * @return plane's range;
     */
    public int getRange() {
        return range;
    }

    /**
     * Function to get plane's fuselage.
     * @return plane's fuselage;
     */
    public int getFuselage() {
        return fuselage;
    }

    /**
     * Function to set plane's fuselage.
     * @param fuselage is the value of fuselage to be set.
     */
    public void setFuselage(int fuselage) {
        this.fuselage = fuselage;
    }

    /**
     * Function to set plane's range.
     * @param range is the value of range to be set.
     */
    public void setRange(int range) {
        this.range = range;
    }

    /**
     * Function to get plane's information.
     * @return plane's info.
     */
    public String getInfo(){
        return "\nfuselage = " + fuselage + "\nrange = " + range + "\npayLoad = " + payload;
    }

}


