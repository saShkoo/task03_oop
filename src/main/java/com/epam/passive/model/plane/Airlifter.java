package com.epam.passive.model.plane;

/**
 * Airlifter is a child class of Airline abstract class.
 */
public class Airlifter extends Plane {
    private int extraLoad;

    public Airlifter(int fuselage, int range, int payload, int extraLoad, int speed, int fuel) {
        this.fuselage = fuselage;
        this.range = range;
        this.payload = payload;
        this.extraLoad = extraLoad;
        this.fuel = fuel;
        this.speed = speed;
    }

    /**
     * Function to get Airlifter's payLoad.
     * @return load sum of all planes in Airline.
     */
    public int getPayload() { return payload+extraLoad; }

    /**
     * Function to get Airlifter's extra load.
     * @return Airlifter's extra load.
     */
    public int getExtraLoad() {
        return extraLoad;
    }

    /**
     * Function to set Airlifter's extra load.
     * @param extraLoad is the value of extra load to be set.
     */
    public void setExtraLoad(int extraLoad) {
        this.extraLoad = extraLoad;
    }

    /**
     * Function to get information Airlifter.
     * @return Airlifter;s info.
     */
    public String getInfo() {
        return "\nType: Airlifter" + " | \tspeed(kph): " + speed + " | \tfuel(litres): " + fuel + " | \tfuselage: " + fuselage
                + " | \trange(km): " + range +" | \tpayLoad: " + payload +  " | \tpassengers: " + "-" + " | \textra load: " + extraLoad;
    }
}
