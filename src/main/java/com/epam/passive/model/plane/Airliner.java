package com.epam.passive.model.plane;

/**
 * Airlifter is a child class of Airline abstract class.
 */
public class Airliner extends Plane {
    private int passengers;

    public Airliner(int fuselage, int range, int payload, int passengers, int speed, int fuel)
    {
        this.fuselage = fuselage;
        this.range = range;
        this.payload = payload;
        this.passengers = passengers;
        this.speed = speed;
        this.fuel = fuel;
    }

    /**
     * Function to get count of passengers in Airliner.
     * @return count of passengers in Airliner.
     */
    public int getPassengers() { return passengers; }

    /**
     * Function to set passengers count in Airliner.
     * @param passengers is the number of passengers.
     */
    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    /**
     * Function to get Airliner's information.
     * @return Airliner's info
     */
    public String getInfo() {
        return "\nType: Airliner " + " | " + "\tspeed(kph): "+speed + " | \tfuel(litres): " + fuel + " | \tfuselage: " + fuselage +
                " | \trange(km): " + range + " | \tpayLoad: " + payload + " | \tpassengers: " + passengers + " | \textra load:  -";
    }
}
