package com.epam.passive.model;
import com.epam.passive.model.airline.Airline;
import com.epam.passive.model.airline.Ryanair;
import com.epam.passive.model.plane.Airlifter;
import com.epam.passive.model.plane.Airliner;
import com.epam.passive.model.plane.Plane;
import java.util.List;

public class ModelImpl implements Model {
    private Airline airline;
    public ModelImpl() {
        airline = new Ryanair();
        Airliner t1 = new Airliner(20000, 1000, 20, 350, 750, 20000);
        airline.getPlanes().add(t1);
        Airlifter t2 = new Airlifter(15000, 900, 80, 50, 780, 27876);
        airline.getPlanes().add(t2);
        Airliner t3 = new Airliner(19000, 850, 19, 200, 850,34308);
        airline.getPlanes().add(t3);
        Airlifter t4 = new Airlifter(15000, 950, 100, 20, 903,43890);
        airline.getPlanes().add(t4);
    }
    @Override
    public List<Plane> getPlanes() {
        return airline.getPlanes();
    }
    @Override
    public void sortByRange() {
            airline.sortByRange();
    }
    @Override
    public void findPlaneByFuel(int key) { airline.findPlaneByFuel(key);}
    @Override
    public int getTotalFuselage() {
        return airline.getTotalFuselage();
    }
    @Override
    public int getTotalLoad() { return airline.getTotalLoad(); }
    @Override
    public String outInfo() {
        return airline.outInfo();
    }
}
