package com.epam.passive.model.transport;

/**
 * Abstract class Transport.
 */
public abstract class Transport {
    protected int speed;
    protected int fuel;

    /**
     * Function to get transport's fuel value.
     * @return transport's fuel value.
     */
    public int getFuel() {
        return fuel;
    }

    /**
     * Function to get transport's speed.
     * @return transport' speed.
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Function to set transport's fuel value;
     * @param fuel is a value of fuel to be set.
     */
    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    /**
     * Function to set transport's speed.
     * @param speed is a value of speeed to be set.
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
