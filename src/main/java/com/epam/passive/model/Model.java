package com.epam.passive.model;
import com.epam.passive.model.plane.Plane;
import java.util.List;

public interface Model {
     /**
      * Function to get Airline's PlaneList.
      * @return Airline's PlaneList
      */
     List<Plane> getPlanes();

     /**
      * Function to sort PlaneList my planes' range.
      */
     void sortByRange();

     /**
      * Function to get PlaneList total fuselage.
      * @return fuselage sum of PlaneList.
      */
     int getTotalFuselage();

     /**
      * Function to get PlaneList total load;
      * @return payload sum of PlaneList.
      */
     int getTotalLoad();

     /**
      * Function to find plane by its fuel value.
      * @param key is the fuel value of searched plane.
      */
     void findPlaneByFuel(int key);

     /**
      * Function to get PlaneList information.
      * @return PLaneList's info.
      */
     String outInfo();
}
