package com.epam.passive.model.airline;
import com.epam.passive.model.plane.Plane;
import java.util.ArrayList;

/**
 * Ryanair extends abstract class Airlne.
 */
public class Ryanair extends Airline {
   public Ryanair() {
      planes = new ArrayList<Plane>();
      name = "RyanAir";
   }
}
