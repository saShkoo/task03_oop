package com.epam.passive.model.airline;
import com.epam.passive.model.plane.Plane;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Abstract Airline class.
 */
public abstract class Airline {
    protected String name;
    protected List<Plane> planes;

    /**
     * Function to get the name of Airline.
     * @return Airline's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Function to set Airline's name.
     * @param name is the Airline's name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Function to get the PlaneList of Airline
     * @return PlaneList of Airline.
     */
    public List<Plane> getPlanes() { return planes; }

    /**
     * Function to get the info about Airline's PlaneList.
     * @return PlaneList's info.
     */
    public String outInfo() {
        String str = "";
        str += "\nCompany NAME: " + name;
        str += "\nList of planes:";
        for (Plane i: planes) {
            str += "\nPLANE " + (planes.indexOf(i) + 1);
            str += i.getInfo();
        }
        return str;
    }

    /**
     * Function to set Airline's PlaneList.
     * @param planes PlaneList to set.
     */
    public void setPlanes(List<Plane> planes) { this.planes = planes; }

    /**
     * Function to sort PlaneList by their range.
     */
    public void sortByRange() {
        System.out.println("Plane List before Sort:");
        System.out.println(outInfo());
        Collections.sort(planes, new Comparator<Plane>() {
            @Override
            public int compare(Plane t1, Plane t2) {
                return t1.getRange() < t2.getRange() ? -1: (t1.getRange() < t2.getRange()) ? 1 : 0;
            }
        });
        System.out.println("Plane List after Sort:");
        System.out.println(outInfo());
    }

    /**
     * Function to get total amount of fuselage of PlaneList.
     * @return sum of planes' fuselages.
     */
    public int getTotalFuselage() {
        int sum = 0;
        for(int i = 0; i < planes.size(); i++) {
            sum += planes.get(i).getFuselage();
        }
        return sum;
    }

    /**
     * Function to get total amount of payLoad of PlaneList.
     * @return sum of planes' payLoads.
     */
    public int getTotalLoad() {
        int sum = 0;
        for(int i = 0; i < planes.size(); i++){
            sum += planes.get(i).getPayload();
        }
        return sum;
    }

    /**
     * Function to find the plane with concrete fuel value.
     * @param key is the fuel value of searched plane.
     */
    public void findPlaneByFuel(int key) {
        int index = -1;
        for(int i = 0; i < planes.size(); i++) {
            if((planes.get(i).getFuel()) == key) {
                index = i;
                System.out.println(planes.get(index).getInfo());
            }
        }
        if(index == -1){
            System.out.println("There is no plane with such fuel usage in he company RyanAir");
        }
    }
}