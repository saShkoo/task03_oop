package com.epam.passive.view;
import com.epam.passive.controller.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "  1 - print Airline Company");
        menu.put("2", "  2 - sort planes by range");
        menu.put("3", "  3 - find plane by fuel usage");
        menu.put("4", "  4 - get total fuselage");
        menu.put("5", "  5 - get total load");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<String, Printable>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    /**
     * Function to display Airline's info.
     */
    private void pressButton1() {
        System.out.println("\nCompany INFO:");
        System.out.println(controller.outInfo());
    }

    /**
     * Function to display result of sorting PlaneList by range;
     */
    private void pressButton2() {
        System.out.println("Sorting planes by their range . . .");
        controller.sortByRange();
        controller.outInfo();
    }

    /**
     * Function to display result of searching for plane by its fuel value.
     */
    private void pressButton3() {
        System.out.print("Enter the value of fuel to find the plane: ");
        int amount = Integer.parseInt(input.nextLine());
        controller.findPlaneByFuel(amount);
    }

    /**
     * Function to display total fuselage of PlaneList.
     */
    private void pressButton4() {
        System.out.println("Total fuselage: ");
        System.out.println(controller.getTotalFuselage());
    }

    /**
     * Function to display total payLoad of PlaneList.
     */
    private void pressButton5() {
        System.out.println("Total payLoad: ");
        System.out.println(controller.getTotalLoad());
    }

    /**
     * Function to display menu.
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Function to select menu point.
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println("Unexpected error");
            }
        } while (!keyMenu.equals("Q"));
    }
}
