package com.epam.passive.controller;
import com.epam.passive.model.*;
import com.epam.passive.model.plane.Plane;
import java.util.*;

/**
 * ControllerImpl implements Controller CLass.
 */
public class ControllerImpl implements Controller {
    private Model model;
    public ControllerImpl() { model = new ModelImpl(); }
    @Override
    public List<Plane> getPlanes() { return model.getPlanes(); }
    @Override
    public void sortByRange() { model.sortByRange(); }
    @Override
    public int getTotalFuselage() { return model.getTotalFuselage(); }
    @Override
    public int getTotalLoad() { return model.getTotalLoad(); }
    @Override
    public void findPlaneByFuel(int key) { model.findPlaneByFuel(key); }
    @Override
    public String outInfo() { return model.outInfo(); }
}

