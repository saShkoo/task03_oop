package com.epam.passive.controller;
import com.epam.passive.model.plane.Plane;
import java.util.*;

/**
 * Controller Class is a part of mvc.
 */
public interface Controller {
     /**
      * Function calculates total fuselage of all planes in Airline.
      * @return fuselage sum of all planes in Airline.
      */
     int getTotalFuselage();

     /**
      * Function to calculate total load of all planes in Airline.
      * @return load sum of all planes in Airline.
      */
     int getTotalLoad();

     /**
      * Function to find plane with concrete fuel value.
      * @param key is the searched fuel value of plane.
      */
     void findPlaneByFuel(int key);

     /**
      * Function to sort planes by their range.
      */
     void sortByRange();

     /**
      * Function to get list of planes in Airline.
      * @return plane list of Airline.
      */
     List<Plane> getPlanes();

     /**
      * Function to display PlaneList data.
      * @return PlaneList data.
      */
     String outInfo();
}
