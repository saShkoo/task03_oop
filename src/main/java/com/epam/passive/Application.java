package com.epam.passive;
import com.epam.passive.view.*;

/**
 * @author Olexandra Stan
 * @version 1.0
 */
public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}

